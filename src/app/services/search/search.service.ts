import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SearchResult } from '../../interfaces/search-result/search-result';
import { tap } from 'rxjs/operators/tap';
import { of } from 'rxjs/observable/of';
import { catchError } from 'rxjs/operators/catchError';

@Injectable()
export class SearchService {

  endPoint = 'http://www.mocky.io';

  constructor(private httpClient: HttpClient) { }

  /**
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
private handleError<T> (operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {

    console.error(error); // log to console instead

    // TODO: better job of transforming error for user consumption
    //this.log(`${operation} failed: ${error.message}`);
    
    return of(result as T);
  };
}

  getResults(searchText: string): Observable<SearchResult[]>{

    let apiUrl = `${this.endPoint}/v2/5a58f8952d0000b62fd2e6bb`;

    return this.httpClient.get<SearchResult[]>(apiUrl)
    .pipe( 
      tap( results => console.log("search done from service")),
      catchError(this.handleError('getResults', []))
    );
    
  }

}

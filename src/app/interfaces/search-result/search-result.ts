export interface SearchResult {
    title: string;
    description: string;
    salary: string;
    keySkills: string[];
}

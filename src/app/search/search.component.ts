import { Component, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { SearchService } from '../services/search/search.service';
import { SearchResult } from '../interfaces/search-result/search-result';
import { Observable } from 'rxjs/Observable';
import { catchError, map, tap } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  animations: [
    trigger('showWaitState', [
      state('false', style({
        backgroundColor: '#eee',
        transform: 'scale(0,8)'
      })),
      state('true', style({
        backgroundColor: '#ccc',
        transform: 'scale(1)'
      })),
      transition('false => tue', animate('1000ms ease-in')),
      transition('true => false', animate('1000ms ease-out'))
    ])
  ]
})
export class SearchComponent implements OnInit {

  searchText: string;
  showPleaseWait = false;
  results: Observable<SearchResult[]>;

  constructor(private searchService: SearchService) { }

  ngOnInit() {

  }

  onSearch() {
    this.showPleaseWait = true;
    this.results = this.searchService.getResults(this.searchText);
    this.results.subscribe ( r => this.showPleaseWait = false);
  }

}
